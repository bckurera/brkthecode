# BrkTheCode

The puzzle is in the directory /data, before executing the program adjust the path or copy the file in the /data directory to the location of the sourcecode.

==Puzzle==

IFS Labs has begun to automate their Turkish Delight confectionary. Everything is set to go but the last step of the production process involves adding toppings to individual pieces of Turkish Delight that have varying sizes which roll out on a conveyor belt. 

The amount of toppings that needs to be added is based on three simple but strange rules.
    1. At least 1g of toppings needs to be added to every piece.
    2. If two adjacent pieces are of different sizes, the larger piece needs to have at least 1g more than the smaller one.
    3. If two adjacent pieces are of the same size, then the amount of toppings relative to each other does not matter.
                        
The goal then is to calculate the amount of toppings needed for each piece of Turkish Delight such that the total amount needed for a batch, is minimized.
                        
Assumptions

    1. The smallest unit of toppings is 1g. Each additional unit is a multiple of this amount.
    2. The Turkish Delight pieces come out of the conveyor belt one at a time.
                        
Example
    
    Sample Data Set<br>
        {
        "id" : ID,
        "generatedOn" : DATE,
        "data" : [ 1, 2, 2, 6, 2, 1 ]
        }
    
The index of the data array is the order of which the pieces are arranged on the conveyor belt. The content of the array indicates how big the Turkish Delight piece is. The above can be graphically represented as:
                
    The 1st piece of size 1 would need 1g.
    The 2nd piece is larger than the 1st piece so it needs at least 2g.
    The 3rd piece is the same size as the 2nd piece so according to the rules, the amount of toppings relative to each other does not matter + the end goal is to minimize the amount of toppings so we can get away by just adding 1g.
    The 4th piece is larger than the previous. So, we could theoretically add 2g even though it’s 3 times larger than the previous piece, the rules do not state that we need to add 3 time more, but if you look at the 5th and 6th pieces they get smaller and smaller so the 6th piece will end up with no toppings! This violates the 1st rule. So, the amount needed for the 4th piece should be at least 3g.
    
    The minimum total toppings required is then 1 + 2 + 1 + 3 + 2 + 1 = 10g.
        
    The expected output is a .txt file in JSON format.
    
    {
    "answer" : [ 1 , 2, 1, 3, 2, 1 ]
        }