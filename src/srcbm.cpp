/*
IFS Break the Code Chllange - http://brkthecode.com/
Author - Buddhika Kurera
Date   - 2019-11-10
*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include "nlohmann/json.hpp"

using json = nlohmann::json;
using namespace std;

/*
Class : util - use to read and write file
*/
class util{
    public:

    //conver a vector to a comma seperated string
    string str_vector(vector<int> v){
        string s = "";
        for (int i; i < v.size(); i++){
            s = s + std::to_string(v[i]) + "," ;
        }
        int l = s.length();
        return s.substr(0, l-1);;
    }

    //write a string to a file, format JSON
    void write_to_file(string s){
        ofstream myfile;
        myfile.open ("answer.txt");
        myfile << "{\"answer\" : [" << s << "]}";
        myfile.close();
    }

    //open file and read the file, JSON format
    string open_file(){
        string line;
        string text;
        ifstream myfile ("IFSLabs_BrkTheCode_DataSet.txt");
        if (myfile.is_open()){
            while ( getline (myfile,line) ){
                text = text + line;
            }
            myfile.close();
        }
        return text;
    }

};

int main(){
    //Initialization of the data sequence, sizes of cookies
    vector<int> sequence_date;

    util obj = util();

    string text = obj.open_file();

    auto j = json::parse(text);

    for(int x=0; x < j["data"].size(); x++){
        sequence_date.push_back(j["data"][x]);
    }
    
    //curr_value and next_value use to hold two adjacent value of the sequence_data vector
    int curr_value = 0;
    int next_value= 0;

    vector<int> output;

    int data_size = sequence_date.size();
    output.assign(data_size, 1);

    //Implement the main logic
    for(int i = 0; i < (data_size-1) ; i++){

        //read the values from the sequence_data indexed by i and i+1
        curr_value = sequence_date[i];
        next_value = sequence_date[i+1];
        
        //if the fwd adjacent value is greater than the current value
        //output of the fwd adjacent value is increased by 1
        if(curr_value < next_value){
            output[i+1] = output[i] + 1;
        }

        //if the fwd adjacent value is equal to the current value
        //output of the fwd adjacent value is set to 1
        if(curr_value == next_value){
            output[i+1] = 1;
        }

        //if the fwd adjacent vaue is lesser than the current value
        //check if the current value of the output is 1, if so goes to backward correction
        //if the output of the current value is greater than 1, output of the fwd adjacent value is set to 1 directly
        if( curr_value > next_value){

            if(output[i] == 1){
                output[i+1] = 1;

                //init the backward correction, set a cursor in the fwd adjacent index in both sequence_date and output vectors
                //propagate the index bacward checking the inconsistent values and correcting them
                for(int j = i+1; j >0 ; j--){
                    int opr = output[j];
                    int oprmin = output[j-1];
                    int qr = sequence_date[j];
                    int qrmin = sequence_date[j-1];

                    //determine the inconsistency, comparing values in sequence_date and output 
                    if((qrmin > qr) and (oprmin <= opr)){
                        output[j-1] = output[j-1] + 1;
                    }

                    //identify the last inconsitent point and exit the loop
                    if(qrmin < qr){
                        break;
                    }
                }
            }else{
                output[i+1] = 1;
            }
        }
    }

    //convert the output vector to string and write to the file
    string st = obj.str_vector(output);
    obj.write_to_file(st);
    return 0;
}
