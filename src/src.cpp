/*
IFS Break the Code Challange - http://brkthecode.com/
Author - Buddhika Kurera
Date   - 2019-11-10
*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <nlohmann/json.hpp>

#define STEP 1

using json = nlohmann::json;
using namespace std;


class agent{
    /*
    Class : agent - the datastructure to store an instance 
    */

    public:

    int agent_id = 0;
    int cookie_size = 0;
    int drop_size = 0;
    bool is_changed = false;
    bool law_violation = false;
    bool slope = false;

    void increase_drop_size(){
        this->drop_size = this->drop_size + STEP;
    }

    void increase_drop_size(int amin_drop_size){
        this->drop_size = amin_drop_size + STEP;
    }

    void decrease_drop_size(int amin_drop_size){
        if(amin_drop_size > 1){
            this->drop_size = amin_drop_size - STEP;
        }
    }

    void decrease_drop_size(){
        if(this->drop_size > 1){
            this->drop_size = this->drop_size - STEP;
        }
    }
};

class util{
    /*
    Class : util - use to read and write file
    */

    public:

    void vis_vector(vector<int> v, std::string vec_name){
        std::cout << vec_name << " - [" ;
        for (int i; i < v.size(); i++){
            std::cout << v[i] << "," ;
        }
        std::cout << "]\n" << flush;
    }


    //conver a vector to a comma seperated string
    string str_vector(vector<agent> v){
        string s = "";
        for (int i; i < v.size(); i++){
            agent a = v[i];
            s = s + std::to_string(a.drop_size) + "," ;
        }
        int l = s.length();
        return s.substr(0, l-1);;
    }

    //write a string to a file, format JSON
    void write_to_file(string s){
        ofstream myfile;
        myfile.open ("answer.txt");
        myfile << "{\"answer\" : [" << s << "]}";
        myfile.close();
    }

    //open file and read the file, JSON format
    string open_file(){
        string line;
        string text;
        ifstream myfile ("IFSLabs_BrkTheCode_DataSet.txt");        
        
        if (myfile.is_open()){
            while ( getline (myfile,line) ){
                text = text + line;
            }
            myfile.close();
        }
        return text;
    }

};

class universe{

public:
  
    bool static check_law_violations(agent *a, agent *amin){

    /*
    Three laws governing the universe:
    1. At least 1g of toppings needs to be added to every piece.
    2. If two adjacent pieces are of different sizes, the larger piece needs to have at least 1g more than the smaller one.
    3. If two adjacent pieces are of the same size, then the amount of toppings relative to each other does not matter.
    Representation is based on the cookie_size and drop_size
    */

        int amin_cookie_size = (*amin).cookie_size;
        int a_cookie_size = (*a).cookie_size;
        int amin_drop_size= (*amin).drop_size;
        int a_drop_size = (*a).drop_size;

        //Identifies a change (^) of the cookie size
        if(amin_cookie_size > a_cookie_size){
            (*a).slope = true;
        }

        //Identifies a change (^) of the cookie_size and change (v) of the drop_size
        if(amin_cookie_size < a_cookie_size){
            if(amin_drop_size >= a_drop_size ){
                (*a).law_violation = true;
                return true;
            }
        }
        
        //Identifies a change (v) of the cookie_size and change (^) of the drop_size
        if(amin_cookie_size > a_cookie_size){
            if(amin_drop_size <= a_drop_size){
                return true;
            }
        }
        
        return false;
    }

};


int main(){
    //Init the data sequence, sizes of cookies
    vector<agent> seq;

    util obj = util();

    string text = obj.open_file();

    auto j = json::parse(text);

    for(int x=0; x < j["data"].size(); x++){
        agent a = agent();
        a.agent_id = x+1;
        a.cookie_size = j["data"][x];
        a.drop_size = 1;
        seq.push_back(a);
    }

    bool found_issue = false;
    int correction_cycle = 0;

    //The main loop starting from the second agent
    for(int x = 1; x < seq.size(); ){
        agent *a = &seq[x];
        agent *amin = &seq[x-1];

        found_issue = universe::check_law_violations(a, amin);

        //fixing the identified violation
        if(found_issue){
            if((*a).slope){
                //(*amin).increase_drop_size();
                (*amin).increase_drop_size();
                (*a).slope = false;
                x--;
            }

            if((*a).law_violation){
                int temp = (*amin).drop_size;

                switch(correction_cycle){
                    case 0:
                    (*a).decrease_drop_size(temp);
                    correction_cycle++;
                    break;

                    case 1:
                    (*a).increase_drop_size(temp);
                    correction_cycle++;
                    break;
                }
            }
        }
        if(!found_issue){
            x++;
            correction_cycle = 0;
        }
    }
    
    //convert the output vector to string and write to the file
    string st = obj.str_vector(seq);
    cout << "Solution : " << st;
    obj.write_to_file(st);
    return 0;
}
